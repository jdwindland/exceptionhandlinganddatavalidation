import java.util.*;
import java.lang.*;

public class ExceptionHandling{

    //Method to divide two numbers.
    public static float doDivision(float num1, float num2) throws ArithmeticException{
        float quotient = num1 / num2;
        //Check to see if the result is Infinity. If it is Infinity, then throw and Arithmetic Exception.
        //This step is needed because I am working with floats instead of ints.
        boolean infinite = Float.isInfinite(quotient);
        if(infinite){
            throw new ArithmeticException();
        }
        return quotient;
    }

    //Main method of program.
    public static void main(String[] args){
        //This variable is needed to exit the while loop.
        boolean pass = false;
        System.out.println("Let's do some division.");
        //While loop to allow the the numbers to be re-entered until the divisor is not 0.
        while (!pass) {
            Scanner systemInScanner = new Scanner(System.in);
            System.out.print("Enter the first number: ");
            float firstNum = systemInScanner.nextFloat();
            System.out.print("Enter the second number: ");
            float secondNum = systemInScanner.nextFloat();
            //Try statement to run the doDivision method and see if it throws an ArithmeticException.
            try {
                float result = doDivision(firstNum, secondNum);
                System.out.println("The result is: " + result);
                pass = true;
            //Catch statement to capture the exception and display user friendly message on what the issue is.
            } catch (ArithmeticException ae) {
                System.out.println("Can't divide by 0.");
            //Finally statement that will always print regardless on if there was an exception.
            } finally {
                //If statement to display a different message depending on if the user entered a valid divisor.
                if(!pass){
                    System.out.println("Re-enter the numbers.");
                }else{
                    System.out.println("CONGRATULATIONS!!! You have divided the numbers.");
                }
            }
        }
    }
}
