import java.util.*;

public class DataValidation{

    //Method to divide two numbers.
    public static float doDivision(float num1, float num2){
        return num1 / num2;
    }

    //Main method of program.
    public static void main(String[] args){
        //This variable is needed to exit the while loop.
        boolean pass = false;
        System.out.println("Let's do some division.");
        //While loop to allow the the numbers to be re-entered until the divisor is not 0.
        while (!pass) {
            Scanner systemInScanner = new Scanner (System.in);
            System.out.print("Enter the first number: ");
            float firstNum = systemInScanner.nextFloat();
            System.out.print("Enter the second number: ");
            float secondNum = systemInScanner.nextFloat();
            //If statement to validate that the divisor is not 0.
            if (secondNum != 0){
                float result = doDivision(firstNum, secondNum);
                System.out.println("The result is: " + result);
                System.out.println("CONGRATULATIONS!!! You have divided the numbers.");
                pass = true;
            }else{
                System.out.println("Can't divide by 0.");
                System.out.println("Re-enter the numbers.");
            }
        }
    }
}